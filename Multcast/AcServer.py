import pickle
import socket
import struct
import message_pb2
from objects import ArCondicionado

ar_condicionado = ArCondicionado(1, 25)

multicast_group = '224.3.29.71'
server_address = ('', 10000)

multicast_server = ('224.3.29.71', 10000)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to
# the multicast group on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(
    socket.IPPROTO_IP,
    socket.IP_ADD_MEMBERSHIP,
    mreq)

# Receive/respond loop
while True:
    print('\nwaiting to receive message')
    data, address = sock.recvfrom(4096)
    # print(data)
    print(address)
    message = pickle.loads(data)
    print(f'Recebido {message.id_command}')
    if message.id_command == 0:
        msg = "ArCondicionado"
        sock.sendto(bytes(msg, "utf-8"), address)
    elif message.id_obj == "All" or message.id_obj == "Arcondicionado":
        if message.id_command == 1:
            msg = ar_condicionado.state()
            sock.sendto(bytes(msg, "utf-8"), address)
        if message.id_command == 3:
            print("Command = 3")
            msg = ar_condicionado.change_state()
            sock.sendto(bytes(msg, "utf-8"), address)
        if message.id_command == 5:
            print("Command = 5")
            msg = ar_condicionado.incrementar_temperatura(message.paramater)
            obj_message = pickle.dumps(msg)
            sock.sendto(bytes(msg, "utf-8"), address)
        if message.id_command == 6:
            print("Command = 6")
            msg = ar_condicionado.incrementar_temperatura(0)
            sock.sendto(bytes(msg, "utf-8"), address)
    else:
        msg = ar_condicionado.incrementar_temperatura(0)
        sock.sendto(bytes(msg, "utf-8"), address)

    # print('sending acknowledgement to', address)
    # sock.sendto(b'ack', address)
