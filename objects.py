class Lampada:
    def __init__(self,id):
        self.id = id
        self.isOn = True
        self.name = "Lâmpada"

    def change_state(self):
        self.isOn = not self.isOn
        if self.isOn:
            return f"A lâmpada {self.id} agora está acessa \n "
        else:
            return f"A lâmpada {self.id} agora está apagada \n"

    def state(self):
        if self.isOn:
            return f"A lâmpada {self.id} está acessa \n"
        else:
            return f"A lâmpada {self.id} está apagada \n"


class ArCondicionado:
    def __init__(self, id, temperature):
        self.id = id
        self.isOn = True
        self.temperature = temperature
        self.name = "Ar-Condicionado"

    def change_state(self):
        self.isOn = not self.isOn
        if self.isOn:
            return f"O ar-condicionado {self.id} agora está ligado com temperatura {self.temperature} \n"
        else:
            return f"O ar-condicionado {self.id} agora está desligado \n"

    def state(self):
        if self.isOn:
            return f"O ar-condicionado  {self.id} está ligado com temperatura {self.temperature} \n"
        else:
            return f"O ar-condicionado {self.id} está desligado \n"

    def incrementar_temperatura(self, input):
        if self.isOn:
            self.temperature = self.temperature + input
            return f"A temperatura atual do ar-condicionado {self.id} é {self.temperature} \n"
        else:
            return f"O ar-condicionado {self.id} está desligado \n"


class Tv:
    def __init__(self, id,chanel):
        self.id = id
        self.isOn = True
        self.chanel = chanel
        self.name = "Televisao"

    def change_state(self):
        self.isOn = not self.isOn
        if self.isOn:
            return f"A Tv {self.id} agora está ligada no canal {self.chanel} \n"
        else:
            return f"A Tv {self.id} agora está desligado"

    def state(self):
        if self.isOn:
            return f"A Tv  {self.id} está ligada no canal {self.chanel} \n"
        else:
            return f"A Tv {self.id} está desligada"

    def change_chanel(self, chanel):
        if self.isOn:
            self.chanel = chanel
            return f"A Tv está no canal {self.chanel} \n"
        else:
            return f"A Tv {self.id} está desligada \n"
