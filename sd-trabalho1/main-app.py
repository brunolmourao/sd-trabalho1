import pickle
import socket

import message_pb2

port = 1234
server = "localhost"
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((server, port))
print("Bem vindo ao gerenciador de equipamentos inteligentes")
while True:
    print("O que deseja fazer ?")
    print("1 - Estado atual dos equipamentos")
    print("2 - Lista dos equipamentos")
    print("3 - Ligar/Desligar Equipamento")
    print("4 - Mudar Canal (Televisão)")
    print("5 - Mudar Temperatura (Ar-Condicionado)")
    print("6 - Temperatura Atual (Ar-Condicionado)")
    print("7 - Sair ")
    message = input("Escolha uma opção :")
    if message == "1" or message == "2":
        message1 = message_pb2.Message()
        message1.type = "Response"
        message1.id_command = int(message)
        message1.id_obj = "All"
        obj_message = pickle.dumps(message1)
        s.send(obj_message)
        new_msg = True
        while new_msg:
            msg = s.recv(4096)
            print(msg.decode("utf-8"))
            new_msg = False
    elif message == "3" or message == "4" or message == "5" or message == "6":
        equipamento = input("Qual o equipamento ?")
        message1 = message_pb2.Message()
        message1.id_obj = equipamento
        if message == "6":
            message1.type = "Response"
        else:
            message1.type = "Request"
        if message == "4":
            parametro = input("Qual o canal da Televisão?")
            message1.paramater = int(parametro)
        if message == "5":
            parametro = input("Digite quanto deseja modificar a temperatura?")
            message1.paramater = int(parametro)
        message1.id_command = int(message)
        obj_message = pickle.dumps(message1)
        s.send(obj_message)
        new_msg = True
        while new_msg:
            msg = s.recv(4096)
            print(msg.decode("utf-8"))
            new_msg = False
    elif message == "7":
        s.send(bytes(message, "utf-8"))
        s.shutdown(1)
        s.close()
        break
    else:
        print("Valor inváildo")
