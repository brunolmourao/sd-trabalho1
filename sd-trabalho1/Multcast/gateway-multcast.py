import socket
import struct
import pickle
import message_pb2
import sys
list_obj = []
# Socket do Servidor Gateway
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # socket do servidor
s.bind(('localhost', 1234))
s.listen(5)

# message = b'very important data'
multicast_group = ('224.3.29.71', 10000)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set a timeout so the socket does not block
# indefinitely when trying to receive data.
sock.settimeout(0.2)

# Set the time-to-live for messages to 1 so they do not
# go past the local network segment.
ttl = struct.pack('b', 1)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
print(f"Identificando Dispositivos ")
message = message_pb2.Message()
message.type = "GET"
message.id_command = 0
obj_message = pickle.dumps(message)
sent = sock.sendto(obj_message, multicast_group)
while True:
    print('waiting to receive')
    try:
        data, server = sock.recvfrom(4096)

    except socket.timeout:
        print('timed out, no more responses')
        break
    else:
        list_obj.append(data.decode("utf-8"))
        print(data.decode("utf-8"))
        print(list_obj)

while True:
    clientsocket, adress = s.accept()
    print(f"Connection from {adress} has been established")
    while True:
        recebe = clientsocket.recv(4096)
        if not recebe:
            break
        message = pickle.loads(recebe)
        if message.id_command == 2:
            msg = f"0 - {list_obj[0]} \n 1 - {list_obj[1]} \n 2 - {list_obj[2]}"
            clientsocket.send(bytes(msg, "utf-8"))
        else:
            obj_message = pickle.dumps(message)
            msg = ''
            try:
                print(f'Enviando {message.id_command}')
                sent = sock.sendto(obj_message, multicast_group)
                # Look for responses from all recipients
                while True:
                    print('waiting to receive')
                    try:
                        data, server = sock.recvfrom(4096)
                        msg = msg + data.decode("utf-8")
                        print(msg)
                    except socket.timeout:
                        print('timed out, no more responses')
                        break
            finally:
                print(f'Mensagem enviada com sucesso')
                clientsocket.send(msg.encode("utf-8"))
